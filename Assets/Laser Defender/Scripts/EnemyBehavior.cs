﻿using UnityEngine;
using System.Collections;

public class EnemyBehavior : MonoBehaviour {

    public GameObject projectile;
    public float health = 500f;
    public float shotsPerSecond = 0.5f;
    public float enemyProjectileSpeed = 1f;
    public int scoreValue = 175;
    public AudioClip laserSound;
    public AudioClip deathSound;

    private ScoreKeeper scoreKeeper;

    void Start()
    {
        scoreKeeper = GameObject.Find("Score").GetComponent<ScoreKeeper>();

    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        Projectile missile = collider.gameObject.GetComponent<Projectile>();
        if (missile)
        {
            health -= missile.GetDamage(); 
            missile.Hit();
            if (health <= 0)
            {
                Die();
            }
        }

    }

    private void Die()
    {
        Destroy(gameObject);
        AudioSource.PlayClipAtPoint(deathSound, transform.position);
        scoreKeeper.Score(scoreValue);
    }

    void Update()
    {
        float probability = Time.deltaTime * shotsPerSecond;
        if (Random.value < probability)
        {
            Fire();
        }
        

    }

    private void Fire()
    {
        //Vector3 startPosition = transform.position + new Vector3(0, -1, 0);
        GameObject laser = Instantiate(projectile, transform.position, Quaternion.identity) as GameObject;
        laser.GetComponent<Rigidbody2D>().velocity = new Vector3(0, -enemyProjectileSpeed, 0);
        AudioSource.PlayClipAtPoint(laserSound, transform.position);

    }
}
