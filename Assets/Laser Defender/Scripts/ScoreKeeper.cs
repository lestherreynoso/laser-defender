﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreKeeper : MonoBehaviour {

    public static int score = 0;
    private Text scoreText;
	// Use this for initialization
	void Start () {
        scoreText =  GetComponent<Text>();
        UpdateText();
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    public void Score(int points)
    {
        score += points;
        UpdateText();
    }

    private void UpdateText()
    {
        scoreText.text = score.ToString();
    }

    public static void Reset()
    {
        score = 0;
    }
}
