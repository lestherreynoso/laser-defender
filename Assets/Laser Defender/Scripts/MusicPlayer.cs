﻿using UnityEngine;
using System.Collections;

public class MusicPlayer : MonoBehaviour {

    static MusicPlayer instance = null;
    public AudioClip startClip;
    public AudioClip gameClip;
    public AudioClip endClip;

    private AudioSource music;
    void Awake()
    {
        if (instance != null) 
        {
            Destroy(gameObject);
            print("Duplicate destroyed");
        }
        else
        {
            instance = this;
            GameObject.DontDestroyOnLoad(gameObject);
            music = GetComponent<AudioSource>();
            music.clip = startClip;
            music.loop = true;
            music.Play();
        }
	}
   
    // Use this for initialization
	void Start () {

	 }
	// Update is called once per frame
	void Update () {
	
	}

    void OnLevelWasLoaded(int level)
    {
        Debug.Log("MusicPlayer: loaded level: " + level);
        if (music)
        {
            if (level == 0)
            {
                music.Stop();
                music.clip = startClip;
                music.Play();
            }
            if (level == 1)
            {
                music.Stop();
                music.clip = gameClip;
                music.Play();
            }
            if (level == 2)
            {
                music.Stop();
                music.clip = endClip;
                music.Play();
            }
        }
    }
}
